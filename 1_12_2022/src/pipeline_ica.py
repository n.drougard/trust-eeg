#!/usr/bin/env python
# coding: utf-8

import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json

from mne.time_frequency import tfr_morlet
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs,
                                corrmap)
# Data folders
path_string = '/home/dcas/n.drougard/Documents/Git/trust-eeg/doi_10.5061_dryad.9pf3t8d__v1/'
#path_string = '/home/leamathieu/Git/Trust_game/trust-eeg/1_12_2022/src'
folders = [x[0] for x in os.walk(path_string) if 'Iterated_raw_eegdata_sub' in x[0]]
folders.sort()

# Vizualisation
duration = 60
proj = False ## TODO?
remove_dc = False ## TODO?
scalings = {'eeg': 1e-4, 'eog': 1e-4}


cnt = 0
for s in folders:
    filename = s.split('_')[-1] + '.cnt'
    print(filename)    
    
    # .fif file present?
    cnt += 1
    if os.path.exists("subjectS" + str(cnt) + ".fif"):
        print("subjectS" + str(cnt) + ".fif already computed. Delete this file if you want it to be re-computed.")
        continue        
    
    # Load .cnt
    datapath = s + '/' + filename
    raw_init = mne.io.read_raw_cnt(datapath, eog=('HEO','VEO'), preload=True)

    ### Montage
    raw_init.drop_channels(['CB1','CB2'])
    new_name = {'FP1':'Fp1', 'FPZ':'Fpz', 'FP2':'Fp2', 'FZ':'Fz', 'FCZ':'FCz', 'CZ':'Cz', 'CPZ':'CPz', 'PZ':'Pz', 'POZ':'POz', 'OZ':'Oz'}
    mne.rename_channels(info=raw_init.info, mapping=new_name, allow_duplicates=False, verbose=None)
    raw = raw_init.copy()
    raw.set_montage('standard_1020')


    ## 1 Merge data
    # already done in the available .cnt files?


    ## 2 Reject poor segment by visual observation
    chan_state = {}
    with open('channel_state.json', 'r') as f:
        chan_state = json.load(f)
    subject = 'S' + str(cnt)
    print("======================")
    print("========= " + subject + " =========")
    print("======================")   

    raw.info['bads'] = chan_state[subject]
    raw.plot(duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings, title='Raw data')
    print("When the bad channels are selected, close the plot window, and then press enter")
    input()
    
    # Print and save
    print('Bad channels:', raw.info['bads'])
    chan_state[subject] = raw.info['bads']
    with open('channel_state.json', 'w') as f:
        json.dump(chan_state,f)

    # Bad channel reconstruction (interpolation)
    eeg_data = raw.copy().pick(picks="eeg")
    eeg_data_interp = eeg_data.copy().interpolate_bads()
    raw_interp = eeg_data_interp.add_channels([raw.copy().pick(picks="eog")])


    ## 3 Notch filter
    raw_notch = raw_interp.copy().notch_filter(freqs=50, notch_widths=7, n_jobs=-1)
    ## TODO change notch_width?
    #raw_notch.plot(title='after notch', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #raw_notch.plot_psd(fmin=2., fmax=150)
    print(raw_notch.info['bads'])


    ## 4 Rereference to M1 + M2
    raw_ref = raw_notch.copy()
    if 'M1' in chan_state[subject] and 'M2' not in chan_state[subject]:
        raw_ref.set_eeg_reference(ref_channels=['M2'])
        print("Bad M1")
    elif 'M2' in chan_state[subject] and 'M1' not in chan_state[subject]:
        raw_ref.set_eeg_reference(ref_channels=['M1'])
        print("Bad M2")
    elif 'M1' not in chan_state[subject] and 'M2' not in chan_state[subject]:
        raw_ref.set_eeg_reference(ref_channels=['M1', 'M2'])
        print("Good M1 & M2")
    else:
        print("WARNING: both references M1 and M2 are bad.")
        exit()        
    #raw_ref.plot(title='after reference', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)


    ## 5 Remove ocular artifact
    print("HIGH-PASS FILTER")
    raw_art = raw_ref.copy().filter(l_freq=1., h_freq=None, n_jobs=-1)
    ica = ICA(random_state=2)
    # TODO max_iter="auto"? 
    print("FIT ICA")
    ica.fit(raw_art)
    # TODO picks="all"?
    explained_var_ratio = ica.get_explained_variance_ratio(raw_art)
    for channel_type, ratio in explained_var_ratio.items():
        print(f"Fraction of {channel_type} variance explained by all components: " f"{ratio}")
    for i in range(ica.n_components_):
        explained_var_ratio = ica.get_explained_variance_ratio(raw_art, components=[i], ch_type="eeg")
        # This time, print as percentage.
        ratio_percent = round(100 * explained_var_ratio["eeg"])
        print(f"Fraction of variance in EEG signal explained by component " f"{i}:" f"{ratio_percent}%")
    #ica.plot_sources(raw_art)
    ica.exclude = []
    # find which ICs match the EOG pattern
    eog_indices, eog_scores = ica.find_bads_eog(raw_art)
    print("eog_indices", eog_indices)
    ica.exclude = eog_indices
    # barplot of ICA component "EOG match" scores
    #ica.plot_scores(eog_scores)
    print("eog_scores", eog_scores)
    # plot diagnostics
    #ica.plot_properties(raw_art, picks=eog_indices)
    # plot ICs applied to raw data, with EOG matches highlighted
    #ica.plot_sources(raw_art)
    ## TODO compare artifact cleaning with SSP & ICA using correlation with HEO & VEO (# compare with SSP for artifact removal)
    ## TODO check PCA parameters (e.g. n_pca_components)
    print('exclude', ica.exclude)
    ica.apply(raw_art)
    #raw_art.plot(title='after artifact removal', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    print("Save .fif file")
    raw_art.save("subject" + subject + ".fif", overwrite=True)
    print("The end")
