#!/usr/bin/env python
# coding: utf-8
# ## Pipeline 

# +
import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json

from mne.time_frequency import tfr_morlet
#from mne.time_frequency import tfr_morlet, psd_multitaper, psd_welch
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs,
                                corrmap)

# +
#Accès aux données
#path_string = '/home/farmbot-server/work_on_git/trust-eeg/doi_10.5061_dryad.9pf3t8d__v1/'
path_string = '/home/dcas/n.drougard/Documents/Git/trust-eeg/doi_10.5061_dryad.9pf3t8d__v1/'
base_string = 'Iterated_raw_eegdata_sub'
# Paramètres de visualisation
duration = 60
proj = False ## TODO vraiment?
remove_dc = False ## TODO vraiment?
scalings = {'eeg': 1e-4, 'eog': 1e-4}

#Sujets sur lesquels itérer
SUBJ_tot = []
n_subjects = 3

for i in range(1, n_subjects+1):
    SUBJ_tot.append(path_string + f'{base_string}{i:02}')
    
print(len(SUBJ_tot))

#Matrice contenant les ERP en temporel : type numpy array
trust_forall_temp = []
all_betrayals_temp =[]
distrust_all_temp = []

#Matrices contenant les ERP en Temps-Fréquence : type numpy array
trust_forall_tf = []
all_betrayals_tf =[]
distrust_all_tf = []

f = open('bad_chan.txt', "w")
f.write('')
f.close()

# +
fEEG = []

for iDir in SUBJ_tot:
    filename = os.listdir(iDir)
    print(filename)
    
    for iFile in filename:
        if iFile.endswith(".cnt"):       
            f = os.path.join(iDir,iFile)
            if os.path.isfile(f):
                fEEG.append(f)
            else :
                    print('Error on path')
# -


print(fEEG)

if not os.path.exists("channel_state.json"): 
    with open("channel_state.json", 'w') as file: 
        file.write("{}") 
else: 
    print(f"The file channel_state.json already exists.") 

for sn in [2]:
#range(len(fEEG)): 
    iEEG = fEEG[sn]
    
    raw_init = mne.io.read_raw_cnt(iEEG, eog=('HEO','VEO'), preload=True)
    print(iEEG)

    ### Preprocessing
    #raw_init.plot(title='Raw data')

    ### Montage
    raw_init.drop_channels(['CB1','CB2'])
    new_name = {'FP1':'Fp1', 'FPZ':'Fpz', 'FP2':'Fp2', 'FZ':'Fz', 'FCZ':'FCz', 'CZ':'Cz', 'CPZ':'CPz', 'PZ':'Pz', 'POZ':'POz', 'OZ':'Oz'}
    mne.rename_channels(info=raw_init.info, mapping=new_name, allow_duplicates=False, verbose=None)
    raw = raw_init.copy()
    raw.set_montage('standard_1020')
    n_chan = len(raw.ch_names)

    ## 1 Merge data

    ## 2 Reject poor segment by visual observation
    with open('channel_state.json', 'r') as f:
        chan_state = json.load(f)
    subject = 'S' + str(sn+1)
    print("======================")
    print("========= " + subject + " =========")
    print("======================")   
    check_chan = False
    if subject in chan_state:
        raw.info['bads'] = chan_state[subject]
        print('Bad channels:', raw.info['bads'])
        print("Enter 'Y' if you want to check the channels:")
        x = input()
        if x == 'Y':
            check_chan = True
    else:
        check_chan = True
        chan_state[subject] = []

    if check_chan:
        print("Manual channel checking starting")
        print(chan_state)

        channel_indices = list(range(n_chan))
        print('Number of channels:', n_chan)

        # Nombre de cannaux par figure
        #n_chan_group = 30
        #n_viz = math.ceil(n_chan / n_chan_group)
        #print('Total number of visualizations:', n_viz)

        # Toutes les électrodes
        raw.plot(duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings, title='Channel checking mode')

        # Boucle pour afficher chaque canal individuellement
        #for viz in list(range(n_viz)):
        #    istop = min((1+viz)*n_chan_group,n_chan)
        #    chan_group = list(range(viz*n_chan_group,istop))
        #    raw.plot(duration=duration, proj=proj, order=chan_group, remove_dc=remove_dc, scalings=scalings)
        #    print("Press enter to continue")
        x = input()
        plt.close()
        chan_state[subject] = raw.info['bads']
        with open('channel_state.json', 'w') as f:
            json.dump(chan_state,f)
    else:
        print("The file channel_state.json is used directly for defining bad channels")

    ## 2extra interpolate bads
    #print('before interp', raw.info)
    eeg_data = raw.copy().pick(picks="eeg")
    eeg_data_interp = eeg_data.copy().interpolate_bads()
    #eeg_data.plot(title='before (eeg_data)', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #print(eeg_data.info['bads'])
    #eeg_data_interp.plot(title='after (eeg_data_interp)', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #print(eeg_data_interp.info['bads'])
    raw_interp = eeg_data_interp.add_channels([raw.copy().pick(picks="eog")])
    #raw_interp.plot(title='after full (raw_interp)', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #print('before interp', raw_interp.info)

    ## 3 Notch filter
    raw_notch = raw_interp.copy().notch_filter(freqs=50, notch_widths=7,n_jobs=-1)
    ## TODO change notch_width?
    #raw_notch.plot(title='after notch', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #raw_notch.plot_psd(fmin=2., fmax=150)

    ## 4 Rereference to M1 + M2
    raw_ref = raw_notch.copy()
    raw_ref.set_eeg_reference(ref_channels=['M1', 'M2'])
    #raw_ref.plot(title='after reference', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)

    ## 5 Remove ocular artifact
    print("HIGH-PASS FILTER")
    raw_art = raw_ref.copy().filter(l_freq=1., h_freq=None, n_jobs=-1)
    #print(raw_art.ch_names)
    #print(n_chan)
    ica = ICA(random_state=2)
    #max_iter="auto", 
    print("FIT ICA")
    ica.fit(raw_art)
    #, picks="all")
    explained_var_ratio = ica.get_explained_variance_ratio(raw_art)
    for channel_type, ratio in explained_var_ratio.items():
        print(f"Fraction of {channel_type} variance explained by all components: " f"{ratio}")
    for i in range(ica.n_components_):
        explained_var_ratio = ica.get_explained_variance_ratio(raw_art, components=[i], ch_type="eeg")
        # This time, print as percentage.
        ratio_percent = round(100 * explained_var_ratio["eeg"])
        print(f"Fraction of variance in EEG signal explained by component " f"{i}:" f"{ratio_percent}%")
    #ica.plot_sources(raw_art)
    ica.exclude = []
    # find which ICs match the EOG pattern
    eog_indices, eog_scores = ica.find_bads_eog(raw_art)
    print("eog_indices", eog_indices)
    ica.exclude = eog_indices
    # barplot of ICA component "EOG match" scores
    #ica.plot_scores(eog_scores)
    print("eog_scores", eog_scores)
    # plot diagnostics
    #ica.plot_properties(raw_art, picks=eog_indices)
    # plot ICs applied to raw data, with EOG matches highlighted
    #ica.plot_sources(raw_art)
    ## TODO compare artifact cleaning with SSP & ICA using correlation with HEO & VEO (# compare with SSP for artifact removal)
    print("The end")
    # TODO check PCA parameters (e.g. n_pca_components)
    print('exclude', ica.exclude)
    ica.apply(raw_art)
    #raw_art.plot(title='after artifact removal', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    raw_art.save("subject" + subject + ".fif", overwrite=True)
