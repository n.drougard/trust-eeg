#!/usr/bin/env python
# coding: utf-8
import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json
import glob
from mne.time_frequency import tfr_morlet, tfr_multitaper
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs, corrmap)
from mne import grand_average

def reject_epochs_by_threshold(epochs_temp, threshold=75.0):
        # Reject epochs based on maximum peak-to-peak signal amplitude (PTP)
        #reject_criteria = dict(eeg=threshold * 1e-6) 
        reject_criteria = dict(eeg=lambda x: ((np.max(np.abs(x), axis=1) > threshold).any(), "max abs amp"))
        epochs_temp.drop_bad(reject=reject_criteria)
        return epochs_temp

# Visualization parameters
duration = 60
proj = False ## TODO?
remove_dc = False ## TODO?
scalings = {'eeg': 1e-4, 'eog': 1e-4}
fifCounter = len(glob.glob1('.',"*.fif"))
print(fifCounter)
if not os.path.exists("plots"):
    os.makedirs("plots")

# FROM PAPER TRUST GAME DATASET
# 1) For the outcome-evaluation stage (i.e., when the participant sees the outcomes), single-trial epochs were extracted from 1,000 ms before to 2,000 ms after each feedback presentation.
# 2) Secondly, using SCAN software (Neuroscan Inc.), the average value of epochs ranging from −200 to 0 ms was subtracted from each epoch and artifact exclusion on .eeg data was performed so that the epochs (trials) in which EEG voltages exceeded a threshold of ±75 μV during recording were excluded.
# 3) Thirdly, the .eeg data were downsampled to 500 Hz, and a Morlet-based wavelet transform procedure implemented in EEGLAB (Version 14_1_1b) was employed. By which, the continuous estimate of time-frequency power in a given frequency band (3–35 Hz) as a function of time between −1,000 and 2,000 ms was obtained (Delorme and Makeig, 2004).
# 4) Fourthly, time-frequency power was normalized with respect to a 400 to 200 ms prestimulus baseline and converted to decibels [10 × log (μV2)].
# 5) Finally, the time-frequency power among multiple trials of the same condition (trust or distrust condition for the decision-making stage; gain or loss condition for the outcome-evaluation stage) were averaged (Makeig et al., 2004), and the time-frequency power datasets (.datersp format) were obtained.


tfr_array = {}
tfr_array['0'] = []
tfr_array['20'] = []

nb_epochs = {}
nb_epochs['0'] = []
nb_epochs['20'] = []

index_list = list(range(fifCounter))
index_list.remove(14)
index_list.remove(18)
# TODO: check subjects and number of epochs per event id
# TODO: pbm subject 3 ? 15, 19? A lot of "Rejecting  epoch based on EEG" and "bad epochs dropped"? CHECK!

# TFR plot range
tmin, tmax = -0.2,1
fmin,fmax= 5,29

print(index_list)
for i in index_list:
    print("\n\n\n\n********************")
    print("******** S" + str(i) + " ********")
    print("********************")
    name = "subjectS" + str(i+1) + ".fif"
    raw_init = mne.io.read_raw_fif(name)
    ## Getting events
    print('Get events / event_dict from annotations')
    events_from_annot, event_dict = mne.events_from_annotations(raw_init)
    print('events_from_annot:', events_from_annot[:10])
    print('event_dict', event_dict)
    
    #Application du mapping à l'objet annotations # TODO: REMOVE?
    mapping = {1:"KEEP", 2:"PAYOFF_0", 3:"PAYOFF_10", 4:"stop", 5:"GIVE", 6:"PAYOFF_20", 7:"start"}
    annot_from_events = mne.annotations_from_events(
        events=events_from_annot, 
        event_desc=mapping, 
        sfreq=raw_init.info['sfreq'],
        orig_time=raw_init.info['meas_date'])    
    #Application des annotations sur l'objet raw # TODO: REMOVE?
    raw_init.set_annotations(annot_from_events)    

    ## Epoching
    # 1) For the outcome-evaluation stage (i.e., when the participant sees the outcomes), single-trial epochs were extracted from 1,000 ms before to 2,000 ms after each feedback presentation
    # 2) Secondly, using SCAN software (Neuroscan Inc.), the average value of epochs ranging from −200 to 0 ms was subtracted from each epoch 
    event_dict = {'KEEP':1, 'PAYOFF_0':2, 'PAYOFF_10':3, 'stop':4, 'GIVE':5, 'PAYOFF_20':6}
    epochs = mne.Epochs(raw_init, events_from_annot, event_id=event_dict, tmin=-1, tmax=2, preload=True, baseline=(-0.2,0))
    epochs = epochs['PAYOFF_0','PAYOFF_20']
    #epochs['PAYOFF_0'].plot()

    ## Artifact exclusion +/- 75uV
    # and artifact exclusion on .eeg data was performed so that the epochs (trials) in which EEG voltages
    # exceeded a threshold of ±75 μV during recording were excluded.
    epochs_clean = reject_epochs_by_threshold(epochs, threshold=75.0)

    ## Downsampling to 500Hz
    # 3) Thirdly, the .eeg data were downsampled to 500 Hz,     
    epochs_resampled = epochs_clean.resample(500)
    nb_epochs['0'].append(len(epochs_resampled['PAYOFF_0']))
    nb_epochs['20'].append(len(epochs_resampled['PAYOFF_20']))    
    # and a Morlet-based wavelet transform procedure implemented in EEGLAB (Version 14_1_1b) was employed. 
    # By which, the continuous estimate of time-frequency power in a given frequency band (3–35 Hz)
    # as a function of time between −1,000 and 2,000 ms was obtained (Delorme and Makeig, 2004).
    freqs = np.arange(3.0, 35.0, 1.0)
    ncs = 2+freqs//2 # TODO different number of cycle per frequency (here as varwin in EEGLAB)
    power = epochs_resampled.compute_tfr('morlet', freqs, average=False,n_jobs=-1, n_cycles=ncs)
    tfr_array['0'].append(power['PAYOFF_0'].average())
    tfr_array['20'].append(power['PAYOFF_20'].average())
    chanel_names = ['Fz','Cz', 'Pz', 'TP7', 'TP8']
    for chan in chanel_names:
        # 4) Fourthly, time-frequency power was normalized with respect to a 400 to 200 ms prestimulus baseline and converted to decibels [10 × log (μV2)]
        fig = power['PAYOFF_0'].average().plot([chan], baseline=(-0.4, -0.2), tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax, mode='ratio', title='Morlet - Subject: ' + str(i+1) + ' Channel: ' + chan + ' Payoff: 0', show=False, dB=True)
        fig[0].savefig('plots/morlet_s' + str(i+1) + '_po_0' + '_chan_' + chan + '.png')
        fig = power['PAYOFF_20'].average().plot([chan], baseline=(-0.4, -0.2), tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax, mode='ratio', title='Morlet - Subject: ' + str(i+1) + ' Channel: ' + chan + ' Payoff: 20', show=False, dB=True)
        fig[0].savefig('plots/morlet_s' + str(i+1) + '_po_20' + '_chan_' + chan + '.png')
    plt.close('all')

# 5) Finally, the time-frequency power among multiple trials of the same condition (trust or distrust condition for the decision-making stage; gain or loss condition for the outcome-evaluation stage) were averaged (Makeig et al., 2004), and the time-frequency power datasets (.datersp format) were obtained.
GA0 = grand_average(tfr_array['0'])
GA20 = grand_average(tfr_array['20'])
chanel_names = ['Fz','Cz', 'Pz', 'TP7', 'TP8']
for chan in chanel_names:
    fig = GA0.plot([chan], baseline=(-0.4, -0.2), tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax, mode='ratio', title='Morlet - GA - Channel: ' + chan + ' Payoff: 0', show=False, dB=True)
    fig[0].savefig('plots/morlet_GA_0_chan_' + chan + '.png')
    fig = GA20.plot([chan], baseline=(-0.4, -0.2), tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax, mode='ratio', title='Morlet - GA - Channel: ' + chan + ' Payoff: 20', show=False, dB=True)
    fig[0].savefig('plots/morlet_GA_20_chan_' + chan + '.png')
plt.close('all')

# stats on subjects
df_nb_epochs = pd.DataFrame.from_dict(nb_epochs)
df_nb_epochs.to_csv('stats_subjects.csv')
    ## DONE effets de bord (re-croper comme dans le papier)
    ## TODO Payoff 20 seems different from paper: check stats on subjects (min/max values, number of epochs per condition, etc.)
    ## TODO checker le nombre d'epoch par sujet/label TODO (rehab s14-s18) 
    ## TODO CHECKER LA RAM: mem leek (fig?epochs?power?)
    ## TODO different scale with Payoff 0 (-4, 4)
    ## TODO Toutes les images à la même échélle de couleur (comme dans le papier)
    ## TODO créer une figure pour avoir 
    ##         Fz_1    
    ##         Fz_0
    ##  TP7_1  Cz_1  TP8_1
    ##  TP7_0  Cz_0  TP8_0
    ##         Pz_1
    ##         Pz_0
    ## (comme dans /home/dcas/n.drougard/Documents/Git/trust-eeg/1_12_2022/src/plots/DataSheet.PDF )
    ## TODO tests permutation
    ## TODO one shot/iterated -- GAIN/LOSS
