#!/usr/bin/env python
# coding: utf-8
# ## Pipeline

# +
import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json
import glob


from mne.time_frequency import tfr_morlet, tfr_multitaper
#from mne.time_frequency import tfr_morlet, psd_multitaper, psd_welch
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs,
                                corrmap)
# Paramètres de visualisation
duration = 60
proj = False ## TODO vraiment?
remove_dc = False ## TODO vraiment?
scalings = {'eeg': 1e-4, 'eog': 1e-4}
fifCounter = len(glob.glob1('.',"*.fif"))
print(fifCounter)

for i in range(fifCounter):
    name = "subjectS" + str(i+1) + ".fif"
    raw_init = mne.io.read_raw_fif(name)
    #raw_init.plot(duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings, title='With old events')

    ## 1. EPOCH EXTRACTION

    # Repérage des événements
    print('Get events from annotations')
    events_from_annot, event_dict = mne.events_from_annotations(raw_init)
    print('events_from_annot:', events_from_annot)
    # for line in events_from_annot:
    #    print(line)
    print('event_dict:', event_dict)
    # Instanciation du mapping
    mapping = {1: "KEEP", 2: "PAYOFF_0", 3: "PAYOFF_10", 4: "stop", 5: "GIVE", 6: "PAYOFF_20", 7: "start"}

    # Application du mapping à l'objet annotations
    annot_from_events = mne.annotations_from_events(
        events=events_from_annot,
        event_desc=mapping,
        sfreq=raw_init.info['sfreq'],
        orig_time=raw_init.info['meas_date'])

    # Application des annotations sur l'objet raw
    raw_init.set_annotations(annot_from_events)

    #Application des événements trouvés sur raw pour le découper en époques
    # For the DECISION MAKING STAGE
    # only KEEP & GIVE (1 & 5), using the 200ms before as a baseline
    epochs_dm = mne.Epochs(raw_init, events_from_annot, event_id=[1,5], tmin=-0.2, tmax=1, preload=True, baseline=(-0.2,0))

    #Artifact exclusion +/- 75uV
    def reject_epochs_by_threshold(epochs_dm, threshold=75.0):
        # Reject epochs based on maximum peak-to-peak signal amplitude (PTP)
        reject_criteria = dict(eeg=threshold * 1e-6)
        epochs_dm.drop_bad(reject=reject_criteria)
        return epochs_dm

    # Eclusions des époques avec des artéfacts
    print("reject")
    epochs_clean = reject_epochs_by_threshold(epochs_dm, threshold=150.0)
    # Visualisation époques nettoyées
    # epochs_clean.plot()$
    #chanel_names = ['F1', 'F2', 'Fz', 'T7', 'C1', 'Cz', 'C2', 'T8', 'P1', 'P2', 'Pz', 'TP7', 'TP8', 'O1', 'Oz', 'O2']
    #for chan in chanel_names :
    print("plot drop")
    epochs_dm.plot_drop_log()



    #Application des événements trouvés sur raw pour le découper en époques
    # For the OUTCOME EVALUATION STAGE
    # only PAYOFF_0 & PAYOFF_20 (2 & 6), using the 200ms before as a baseline
    epochs_out_eval = mne.Epochs(raw_init, events_from_annot, event_id=[2,6], tmin=-0.2, tmax=1, preload=True, baseline=(-0.2,0))

    #Artifact exclusion +/- 75uV
    def reject_epochs_by_threshold(epochs_out_eval, threshold=75.0):
        # Reject epochs based on maximum peak-to-peak signal amplitude (PTP)
        reject_criteria = dict(eeg=threshold * 1e-6)
        epochs_out_eval.drop_bad(reject=reject_criteria)
        return epochs_out_eval

    # Eclusions des époques avec des artéfacts
    print("reject")
    epochs_clean = reject_epochs_by_threshold(epochs_out_eval, threshold=150.0)
    # Visualisation époques nettoyées
    # epochs_clean.plot()$
    print("plot drop")
    epochs_out_eval.plot_drop_log()

    #Finally, effective trials were superposed and average for the two feedback conditions
    # (trust reciprocity(gain) vs trust betrayal(loss)
    (epochs_out_eval.average())

