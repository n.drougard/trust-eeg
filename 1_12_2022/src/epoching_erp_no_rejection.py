#!/usr/bin/env python
# coding: utf-8
import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json
import glob
from mne.time_frequency import tfr_morlet, tfr_multitaper
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs, corrmap)
from mne import grand_average

chanel_names = ['F1','F2', 'Fz', 'T7', 'C1', 'Cz', 'C2', 'T8', 'P1', 'P2', 'Pz', 'TP7', 'TP8', 'O1', 'O2', 'Oz']

def reject_epochs_by_threshold(epochs_temp, threshold=75.0):
        # Reject epochs based on maximum peak-to-peak signal amplitude (PTP)
        #reject_criteria = dict(eeg=threshold * 1e-6) 
        reject_criteria = dict(eeg=lambda x: ((np.max(np.abs(x), axis=1) > threshold).any(), "max abs amp"))
        epochs_temp.drop_bad(reject=reject_criteria)
        return epochs_temp

# Visualization parameters
duration = 60
proj = False ## TODO?
remove_dc = False ## TODO?
scalings = {'eeg': 1e-4, 'eog': 1e-4}
fifCounter = len(glob.glob1('.',"*.fif"))
print(fifCounter)
if not os.path.exists("plots"):
    os.makedirs("plots")

# FROM PAPER TRUST GAME DATASET
# 1) epochs were extracted from each participant's .cnt data from 200 ms before to 1000 ms after each feedback presentation at first. 
# 2) Then, the baseline correction was performed by subtracting the average value of epochs ranging from −200 to 0 ms (outcome onset) from each epoch. After that, the epochs (trials) in which EEG voltages exceeded a threshold of ±75 μV during recording were excluded. 
# 3) Finally, effective trials of each participant were superposed and averaged for the two feedback conditions [i.e., trust reciprocity (gain) vs. trust betrayal (loss)] and ERPs datasets (.avg format) of the outcome-evaluation stage were got.


erp_array = {}
erp_array['0'] = []
erp_array['20'] = []
index_list = list(range(fifCounter))
#index_list.remove(14)
#index_list.remove(18)
# TODO: check subjects and number of epochs per event id
# TODO: pbm subject 3 ? 15, 19? A lot of "Rejecting  epoch based on EEG" and "bad epochs dropped"? CHECK!


print(index_list)
for i in index_list:
    print("\n\n\n\n********************")
    print("******** S" + str(i) + " ********")
    print("********************")
    name = "subjectS" + str(i+1) + ".fif"
    raw_init = mne.io.read_raw_fif(name)
    ## Getting events
    print('Get events / event_dict from annotations')
    events_from_annot, event_dict = mne.events_from_annotations(raw_init)
    print('events_from_annot:', events_from_annot[:10])
    print('event_dict', event_dict)
    
    #Application du mapping à l'objet annotations # TODO: REMOVE?
    mapping = {1:"KEEP", 2:"PAYOFF_0", 3:"PAYOFF_10", 4:"stop", 5:"GIVE", 6:"PAYOFF_20", 7:"start"}
    annot_from_events = mne.annotations_from_events(
        events=events_from_annot, 
        event_desc=mapping, 
        sfreq=raw_init.info['sfreq'],
        orig_time=raw_init.info['meas_date'])    
    #Application des annotations sur l'objet raw # TODO: REMOVE?
    raw_init.set_annotations(annot_from_events)    

    ## Epoching
    # 1) For the outcome-evaluation stage (i.e., when the participant sees the outcomes), single-trial epochs were extracted from 1,000 ms before to 2,000 ms after each feedback presentation
    # 2) Secondly, using SCAN software (Neuroscan Inc.), the average value of epochs ranging from −200 to 0 ms was subtracted from each epoch 
    event_dict = {'KEEP':1, 'PAYOFF_0':2, 'PAYOFF_10':3, 'stop':4, 'GIVE':5, 'PAYOFF_20':6}
    epochs = mne.Epochs(raw_init, events_from_annot, event_id=event_dict, tmin=-1, tmax=2, preload=True, baseline=(-0.2,0))
    epochs = epochs['PAYOFF_0','PAYOFF_20']
    #epochs['PAYOFF_0'].plot()

    ## Artifact exclusion +/- 75uV
    # and artifact exclusion on .eeg data was performed so that the epochs (trials) in which EEG voltages
    # exceeded a threshold of ±75 μV during recording were excluded.
    epochs_clean = reject_epochs_by_threshold(epochs, threshold=75.0)


    ## 3) Finally, effective trials of each participant were superposed and averaged for the two feedback conditions [i.e., trust reciprocity (gain) vs. trust betrayal (loss)] and ERPs datasets (.avg format) of the outcome-evaluation stage were got.
    ## DONE plot avec les deux conditions
    ## (TODO) donner toutes les électrodes à la fois (pour éviter le message d'erreur, et éventuellement avec les topomaps)
    erp_array['0'].append(epochs['PAYOFF_0'].average())
    erp_array['20'].append(epochs['PAYOFF_20'].average())
    for chan in chanel_names:
        #fig = epochs['PAYOFF_0'].average().plot([chan], show=False)
        #fig.savefig('plots/ERP_s' + str(i+1) + '_po_0' + '_chan_' + chan + '.png')
        #fig = epochs['PAYOFF_20'].average().plot([chan], show=False)
        #fig.savefig('plots/ERP_s' + str(i+1) + '_po_20' + '_chan_' + chan + '.png')
        evokeds = dict(
            po0 =list(epochs['PAYOFF_0'].iter_evoked()),
            po20 =list(epochs['PAYOFF_20'].iter_evoked()),
        )
        fig = mne.viz.plot_compare_evokeds(evokeds, combine="mean", picks=chan, show=False)
        fig[0].savefig('plots/ERP_s' + str(i+1) + '_chan_' + chan + '.png')
    plt.close('all')

# 5) TODO GRAND AVERAGE
GA0 = grand_average(erp_array['0'])
GA20 = grand_average(erp_array['20'])
for chan in chanel_names:
#    fig = GA0.plot([chan], titles='Erp - GA - Channel: ' + chan + ' Payoff: 0', show=False)
#    fig.savefig('plots/erp_GA_0_chan_' + chan + '.png')
#    fig = GA20.plot([chan], titles='Erp - GA - Channel: ' + chan + ' Payoff: 20', show=False)
#    fig.savefig('plots/erp_GA_20_chan_' + chan + '.png')
    evokeds = dict(
        po0 = erp_array['0'],
        # TODO: CORRIGER
        # AttributeError: 'list' object has no attribute 'iter_evoked'
        po20 = erp_array['20'],
    )
    fig = mne.viz.plot_compare_evokeds(evokeds, combine="mean", picks=chan, show=False)
    fig[0].savefig('plots/ERP_GA_chan_' + chan + '.png')
plt.close('all')
