#!/usr/bin/env python
# coding: utf-8
# ## Pipeline 

# +
import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json


from mne.preprocessing import (
    compute_proj_ecg,
    compute_proj_eog,
    create_ecg_epochs,
    create_eog_epochs,
)

from mne.time_frequency import tfr_morlet
#from mne.time_frequency import tfr_morlet, psd_multitaper, psd_welch
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs,
                                corrmap)

# +
#Accès aux données
#path_string = '/home/dcas/n.drougard/Documents/Git/trust-eeg/doi_10.5061_dryad.9pf3t8d__v1/'
path_string = '/home/farmbot-server/work_on_git/trust-eeg/doi_10.5061_dryad.9pf3t8d__v1/'
base_string = 'Iterated_raw_eegdata_sub'
# Paramètres de visualisation
duration = 60
proj = False ## TODO vraiment?
remove_dc = False ## TODO vraiment?
scalings = {'eeg': 1e-4, 'eog': 1e-4}

#Sujets sur lesquels itérer
SUBJ_tot = []

for i in range(1, 3):
    SUBJ_tot.append(path_string + f'{base_string}{i:02}')
    
print(len(SUBJ_tot))

#Matrice contenant les ERP en temporel : type numpy array
trust_forall_temp = []
all_betrayals_temp =[]
distrust_all_temp = []

#Matrices contenant les ERP en Temps-Fréquence : type numpy array
trust_forall_tf = []
all_betrayals_tf =[]
distrust_all_tf = []

f = open('bad_chan.txt', "w")
f.write('')
f.close()

# +
fEEG = []

for iDir in SUBJ_tot:
    filename = os.listdir(iDir)
    print(filename)
    
    for iFile in filename:
        if iFile.endswith(".cnt"):       
            f = os.path.join(iDir,iFile)
            if os.path.isfile(f):
                fEEG.append(f)
            else :
                    print('Error on path')
# -


print(fEEG)
# TODO: creation of an empty json file if it does not exist

for i in range(len(fEEG)): 
    
    iEEG = fEEG[i]
    
    raw_init = mne.io.read_raw_cnt(iEEG, eog=('HEO','VEO'), preload=True)
    print(iEEG)

    ### Preprocessing

    ### Montage
    raw_init.drop_channels(['CB1','CB2'])
    new_name = {'FP1':'Fp1', 'FPZ':'Fpz', 'FP2':'Fp2', 'FZ':'Fz', 'FCZ':'FCz', 'CZ':'Cz', 'CPZ':'CPz', 'PZ':'Pz', 'POZ':'POz', 'OZ':'Oz'}
    mne.rename_channels(info=raw_init.info, mapping=new_name, allow_duplicates=False, verbose=None)
    raw = raw_init.copy()
    raw.set_montage('standard_1020')
    fig_fit = raw.plot_sensors(show_names=True)

    ## 1 Merge data

    ## 2 Reject poor segment by visual observation
    with open('channel_state.json', 'r') as f:
        chan_state = json.load(f)
    subject = 'S' + str(i+1)
    check_chan = False
    if subject in chan_state:
        raw.info['bads'] = chan_state[subject]
        print('Bad channels:', raw.info['bads'])
        print("Enter 'Y' if you want to check the channels:")
        x = input()
        if x == 'Y':
            check_chan = True
    else:
        check_chan = True
        chan_state[subject] = []

    if check_chan:
        print("Manual channel checking starting")
        print(chan_state)

        n_chan = len(raw.ch_names)
        channel_indices = list(range(n_chan))
        print('Number of channels:', n_chan)

        # Nombre de cannaux par figure
        n_chan_group = 30
        n_viz = math.ceil(n_chan / n_chan_group)
        print('Total number of visualizations:', n_viz)

        # Toutes les électrodes
        raw.plot(duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)

        # Boucle pour afficher chaque canal individuellement
        #for viz in list(range(n_viz)):
        #    istop = min((1+viz)*n_chan_group,n_chan)
        #    chan_group = list(range(viz*n_chan_group,istop))
        #    raw.plot(duration=duration, proj=proj, order=chan_group, remove_dc=remove_dc, scalings=scalings)
        #    print("Press enter to continue")
        #    x = input()
        #    plt.close()
        chan_state[subject] = raw.info['bads']
        with open('channel_state.json', 'w') as f:
            json.dump(chan_state,f)
    else:
        print("The file channel_state.json is used for defining bad channels (and not modified)")

    ## 2extra interpolate bads
    #print('before interp', raw.info)
    eeg_data = raw.copy().pick(picks="eeg")
    eeg_data_interp = eeg_data.copy().interpolate_bads()
    #eeg_data.plot(title='before (eeg_data)', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #print(eeg_data.info['bads'])
    #eeg_data_interp.plot(title='after (eeg_data_interp)', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #print(eeg_data_interp.info['bads'])
    raw_interp = eeg_data_interp.add_channels([raw.copy().pick(picks="eog")])
    #raw_interp.plot(title='after full (raw_interp)', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    #print('before interp', raw_interp.info)

    ## 3 Notch filter
    raw_notch = raw_interp.copy().notch_filter(freqs=50, notch_widths=7)
    ## TODO change notch_width?
    raw_notch.plot(title='after notch', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)
    raw_notch.plot_psd(fmin=2., fmax=150)


    ## 4 Rereference to M1 + M2
    raw_ref = raw_notch.copy()
    raw_ref.set_eeg_reference(ref_channels=['M1', 'M2'])
    raw_ref.plot(title='after ref', duration=duration, proj=proj, remove_dc=remove_dc, scalings=scalings)


    ## 5 Remove ocular artifact
    raw_eog_art = raw_ref.copy()
    eog_evoked = create_eog_epochs(raw_eog_art).average(picks="all")
    eog_evoked.apply_baseline((None,None))
    eog_projs, _ = compute_proj_eog(raw_eog_art, n_eeg=1, reject=None, no_proj=True)
    for title in ("Without","With"):
        if title == "With":
            raw_eog_art.add_proj(eog_projs)
        with mne.viz.use_browser_backend("matplotlib"):
            fig = raw_eog_art.plot (duration=duration, proj=True, remove_dc=remove_dc, scalings=scalings)
        fig.subplots_adjust(top=0.9)
        fig.suptitle(f"{title}EOG projectors", size="xx-large", weight="bold")
        input()


    ### DATA POST PROCESSING
    ## Mapping event
    events_from_annot, event_dict = mne.events_from_annotations(raw)
    print(event_dict)
    # Instanciation du mapping
    mapping = {1: "KEEP", 2: "PAYOFF_0", 3: "PAYOFF_10", 4: "stop", 5: "SEND", 6: "PAYOFF_20", 7: "start"}
    raw_pp = raw_eog_art
    annot_from_events = mne.annotations_from_events(
        events=events_from_annot,
        event_desc=mapping,
        sfreq=raw_pp.info['sfreq'],
        orig_time=raw_pp.info['meas_date'])



    ## Epoch extraction for decision making state


    #### Epoched Data
    
    #Repérage des événements
    events_from_annot, event_dict = mne.events_from_annotations(raw)
    print(event_dict)
    #Instanciation du mapping
    mapping = {1:"KEEP", 2:"PAYOFF_0", 3:"PAYOFF_10", 4:"stop", 5:"SEND", 6:"PAYOFF_20", 7:"start"}
    
    #Application du mapping à l'objet annotations
    annot_from_events = mne.annotations_from_events(
        events=events_from_annot, 
        event_desc=mapping, 
        sfreq=raw.info['sfreq'],
        orig_time=raw.info['meas_date'])
    
    #Application des annotations sur l'objet raw
    raw.set_annotations(annot_from_events)    
    #events_from_annot, event_dict = mne.events_from_annotations(raw)
    
    #Application des événements trouvés sur raw pour le découper en époques
    epochs_temp = mne.Epochs(raw, events_from_annot, event_id=event_dict, tmin=-1, tmax=2,
                        preload=True, baseline=(-0.2,0))
    
    #Resample en 500Hz
    epochs_resampled = epochs_temp.resample(500)

    #Time-frequency analysis
    freqs = np.logspace(*np.log10([3, 35]), num=20) #nombre de lignes)
    n_cycles = freqs / 2.  # different number of cycle per frequency
    
    #Transformation de Morlet pour obtenir décomposition en temps-fréquence
    epochs_tf = tfr_morlet(epochs_resampled, freqs=freqs, n_cycles=n_cycles, use_fft=False,
                            return_itc=False, decim=2, n_jobs=1, average=False) #Morlet Transform
        
    #Baseline correction
    epochs_tf.apply_baseline(mode='ratio', baseline=(-0.4,-0.2))

    #Croping
    epochs_tf_croped = epochs_tf.copy().crop(tmin=-0.2, tmax=0.6, include_tmax=True)   

    
    "le dictionnaire est oublié pour une raison inconnue, dans le notebook, il faut le faire tourner 2 fois..."
    #evoked_trust = power['PAYOFF_20']
    
    #ERP en TF
    evoked_trust = epochs_tf_croped['33'] 
    evoked_distrust = epochs_tf_croped['22']
    evoked_betrayal = epochs_tf_croped['11']    
    #Moyenne de tous les ERP du sujet
    evoked_trust_avg = evoked_trust.average()
    evoked_distrust_avg = evoked_distrust.average()
    evoked_betrayal_avg = evoked_betrayal.average()    
    #Choix des 4 chaînes les plus intéressantes
    evoked_trust_avg.pick_channels(['Oz','Cz','Fz','Pz'])
    evoked_distrust_avg.pick_channels(['Oz','Cz','Fz','Pz'])
    evoked_betrayal_avg.pick_channels(['Oz','Cz','Fz','Pz'])   
    #Stockage dans List des ERP en Numpy Array
    trust_forall_tf.append(evoked_trust_avg.data)  
    distrust_all_tf.append(evoked_distrust_avg.data)
    all_betrayals_tf.append(evoked_betrayal_avg.data)
    
    #ERP en temporel
    epochs_temp_trust = epochs_temp['33']
    epochs_temp_distrust = epochs_temp['22']
    epochs_temp_betrayal = epochs_temp['11']
    #Moyenne des ERP Temporels
    epochs_temp_trust_avg = epochs_temp_trust.average()
    epochs_temp_distrust_avg = epochs_temp_distrust.average()
    epochs_temp_betrayal_avg = epochs_temp_betrayal.average()
    #Choix des chaînes
    epochs_temp_trust_avg = epochs_temp_trust_avg.pick_channels(['Oz','Cz','Fz','Pz'])
    epochs_temp_distrust_avg = epochs_temp_distrust_avg.pick_channels(['Oz','Cz','Fz','Pz'])
    epochs_temp_betrayal_avg = epochs_temp_betrayal_avg.pick_channels(['Oz','Cz','Fz','Pz'])
    #Stockage dans liste temporelle
    trust_forall_temp.append(epochs_temp_trust_avg.data)
    all_betrayals_temp.append(epochs_temp_distrust_avg.data)
    distrust_all_temp.append(epochs_temp_betrayal_avg.data)


# +
#copies des listes de données TF
trust_sta_tf=trust_forall_tf.copy()
notrust_sta_tf =trust_forall_tf.copy()
betr_sta_tf =trust_forall_tf.copy()

#Empilement des matrices de la liste temps-fréquence 
METATRUST_TF = np.stack((trust_sta_tf), axis=0)
METABETRAYAL_TF = np.stack((notrust_sta_tf), axis=0)
METADISTRUST_TF = np.stack((betr_sta_tf), axis=0)

#Moyenne des matrices temps-fréquence empilées
METATRUST_TF_avg=METATRUST_TF.mean(axis=0)
METABETRAYAL_avg=METABETRAYAL_TF.mean(axis=0)
METADISTRUST_avg=METADISTRUST_TF.mean(axis=0)

#Check des dimensions
print(METATRUST_TF.shape)
print(METATRUST_TF_avg.shape)

# +
#STOCKAGE DES TF MOYENNÉS DANS CSV
x,y,z = METATRUST_TF_avg.shape
TTF = np.column_stack((np.repeat(np.arange(m),n),METATRUST_TF_avg.reshape(m*n,-1)))
cond_trust_TF_df = pd.DataFrame(TTF)
cond_trust_TF_df.to_csv('trust_TF3.csv')

x,y,z = METADISTRUST_avg.shape
DTF = np.column_stack((np.repeat(np.arange(m),n),METADISTRUST_avg.reshape(m*n,-1)))
cond_trust_TF_df = pd.DataFrame(DTF)
cond_trust_TF_df.to_csv('distrust_TF3.csv')

x,y,z = METABETRAYAL_avg.shape
BTF = np.column_stack((np.repeat(np.arange(m),n),METABETRAYAL_avg.reshape(m*n,-1)))
cond_trust_TF_df = pd.DataFrame(BTF)
cond_trust_TF_df.to_csv('betrayal_TF3.csv')

# +
#COPIES DES LISTES DE DONNÉES Temporelles
trustemp = trust_forall_temp.copy()
dtrusttemp = distrust_all_temp.copy()
betrtemp = all_betrayals_temp.copy()

#Empilement des matrices de la liste temporelle
METATRUST_TEMP = np.stack((trustemp), axis=0)
METADISTRUST_TEMP = np.stack((dtrusttemp),axis=0)
METABETRAYAL_TEMP = np.stack((betrtemp),axis=0)

#Moyenne des matrices temporelles empilées
METATRUST_TEMP_avg= METATRUST_TEMP.mean(axis=0)
METADISTRUST_TEMP_avg= METATRUST_TEMP.mean(axis=0)
METABETRAYAL_TEMP_avg= METATRUST_TEMP.mean(axis=0)

print(METATRUST_TEMP.shape)
print(METATRUST_TEMP_avg.shape)


# +
#STOCKAGE DES TEMPORELS DANS CSV
m,n,r = METATRUST_TEMP.shape
out_arr = np.column_stack((np.repeat(np.arange(m),n),METATRUST_TEMP.reshape(m*n,-1)))
cond_trust_df = pd.DataFrame(out_arr)
cond_trust_df.to_csv('trust_temp3.csv')

m,n,r = METADISTRUST_TEMP.shape
out_arr2 = np.column_stack((np.repeat(np.arange(m),n),METADISTRUST_TEMP.reshape(m*n,-1)))
cond_distrust_df = pd.DataFrame(out_arr2)
cond_distrust_df.to_csv('distrust_temp3.csv')

m,n,r = METABETRAYAL_TEMP.shape
out_arr3 = np.column_stack((np.repeat(np.arange(m),n),METABETRAYAL_TEMP.reshape(m*n,-1)))
cond_betrayal_df = pd.DataFrame(out_arr3)
cond_betrayal_df.to_csv('betrayal_temp3.csv')

# +
#STOCKAGE DES TEMPORELS MOYENNÉS DANS CSV (pas de problème de dimension)
df_trust_temp = pd.DataFrame(METATRUST_TEMP_avg)
df_distrust_temp = pd.DataFrame(METADISTRUST_TEMP_avg)
df_betrayal_temp = pd.DataFrame(METABETRAYAL_TEMP_avg)

df_trust_temp.to_csv('mean_trust_temp3.csv',index=False)
df_distrust_temp.to_csv('mean_distrust_temp3.csv',index=False)
df_betrayal_temp.to_csv('mean_betrayal_temp3.csv',index=False)
