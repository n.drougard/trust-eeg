#!/usr/bin/env python
# coding: utf-8
# ## Pipeline 

import os
import os.path
import math
import numpy as np
import matplotlib.pyplot as plt
import mne
import pandas as pd
import json
import glob


from mne.time_frequency import tfr_morlet, tfr_multitaper
from mne.datasets import somato
from mne.preprocessing import (ICA, create_eog_epochs, create_ecg_epochs,
                                corrmap)
from mne import grand_average

# Paramètres de visualisation
duration = 60
proj = False ## TODO vraiment?
remove_dc = False ## TODO vraiment?
scalings = {'eeg': 1e-4, 'eog': 1e-4}
fifCounter = len(glob.glob1('.',"*.fif"))
print(fifCounter)


if not os.path.exists("plots"):
    os.makedirs("plots")

# FROM PAPER TRUST GAME DATASET
# For the outcome-evaluation stage (i.e., when the participant sees the outcomes), single-trial epochs were extracted from 1,000 ms before to 2,000 ms after each feedback presentation.
# Secondly, using SCAN software (Neuroscan Inc.), the average value of epochs ranging from −200 to 0 ms was subtracted from each epoch and artifact exclusion on .eeg data was performed so that the epochs (trials) in which EEG voltages exceeded a threshold of ±75 μV during recording were excluded.
# Thirdly, the .eeg data were downsampled to 500 Hz, and a Morlet-based wavelet transform procedure implemented in EEGLAB (Version 14_1_1b) was employed. By which, the continuous estimate of time-frequency power in a given frequency band (3–35 Hz) as a function of time between −1,000 and 2,000 ms was obtained (Delorme and Makeig, 2004).
# Fourthly, time-frequency power was normalized with respect to a 400 to 200 ms prestimulus baseline and converted to decibels [10 × log (μV2)].
# Finally, the time-frequency power among multiple trials of the same condition (trust or distrust condition for the decision-making stage; gain or loss condition for the outcome-evaluation stage) were averaged (Makeig et al., 2004), and the time-frequency power datasets (.datersp format) were obtained.



# TODO: pbm subject 3 ? A lot of "Rejecting  epoch based on EEG" and "bad epochs dropped"


tfr_array = {}
tfr_array['0'] = []
tfr_array['20'] = []
index_list = list(range(fifCounter))
print(index_list)
index_list.remove(14)
index_list.remove(18)
print(index_list)
for i in index_list:
    print("\n\n\n\n***********************")
    print("*******S" + str(i) + "************")
    print("***********************")
    name = "subjectS" + str(i+1) + ".fif"
    raw_init = mne.io.read_raw_fif(name)
    #Repérage des événements
    print('Get events from annotations')
    events_from_annot, event_dict = mne.events_from_annotations(raw_init)
    print('events_from_annot:', events_from_annot)
    #for line in events_from_annot:
    #    print(line)
    print('event_dict:', event_dict)
    #Instanciation du mapping
    mapping = {1:"KEEP", 2:"PAYOFF_0", 3:"PAYOFF_10", 4:"stop", 5:"GIVE", 6:"PAYOFF_20", 7:"start"}
    #Application du mapping à l'objet annotations
    annot_from_events = mne.annotations_from_events(
        events=events_from_annot, 
        event_desc=mapping, 
        sfreq=raw_init.info['sfreq'],
        orig_time=raw_init.info['meas_date'])    
    #Application des annotations sur l'objet raw
    raw_init.set_annotations(annot_from_events)    
    
    #epochs_temp = mne.Epochs(raw_init, events_from_annot, event_id=[2,6], tmin=-1, tmax=2, preload=True, baseline=(-0.2,0))
    # For the outcome-evaluation stage (i.e., when the participant sees the outcomes), single-trial epochs were extracted from 1,000 ms before to 2,000 ms after each feedback presentation
    # Secondly, using SCAN software (Neuroscan Inc.), the average value of epochs ranging from −200 to 0 ms was subtracted from each epoch 
    epochs = {}
    epochs['PAYOFF_0'] = mne.Epochs(raw_init, events_from_annot, event_id=[2], tmin=-1, tmax=2, preload=True, baseline=(-0.2,0))
    epochs['PAYOFF_20'] = mne.Epochs(raw_init, events_from_annot, event_id=[6], tmin=-1, tmax=2, preload=True, baseline=(-0.2,0))
    print('EPOCHS')
    print(epochs)
    ### TODO here
    #event_id=[1] for KEEP
    #event_id=[5] for GIVE
    
    

    ##Artifact exclusion +/- 75uV (TODO/MODIFY)
    # TODO: and artifact exclusion on .eeg data was performed so that the epochs (trials) in which EEG voltages exceeded a threshold of ±75 μV during recording were excluded.
    # Définir une fonction pour exclure les époques avec des artéfacts basés sur un seuil
    def reject_epochs_by_threshold(epochs_temp, threshold=75.0):
        # Reject epochs based on maximum peak-to-peak signal amplitude (PTP)
        #reject_criteria = dict(eeg=threshold * 1e-6) 
        reject_criteria = dict(eeg=lambda x: ((np.max(np.abs(x), axis=1) > threshold).any(), "max abs amp"))
        epochs_temp.drop_bad(reject=reject_criteria)
        return epochs_temp
    # Eclusions des époques avec des artéfacts
    print("reject")
    epochs_clean = {}
    for po in ['0','20']:
        print(po)
        epochs_clean['PAYOFF_' + po] = reject_epochs_by_threshold(epochs['PAYOFF_' + po], threshold=75.0)
        
    # Thirdly, the .eeg data were downsampled to 500 Hz, 
    print("downsample")
    # Down sampled 500Hz
    epochs_resampled = {}
    for po in ['0','20']:
        print(po)
        epochs_resampled['PAYOFF_' + po] = epochs_clean['PAYOFF_' + po].resample(500)

    print("Morlet")
    # and a Morlet-based wavelet transform procedure implemented in EEGLAB (Version 14_1_1b) was employed. By which, the continuous estimate of time-frequency power in a given frequency band (3–35 Hz) as a function of time between −1,000 and 2,000 ms was obtained (Delorme and Makeig, 2004).
    freqs = np.arange(3.0, 35.0, 1.0)
    ncs = freqs//2 # TODO different number of cycle per frequency
    for po in ['0','20']:
        print(po)
        power = tfr_morlet(epochs_resampled['PAYOFF_' + po], freqs=freqs, n_cycles=ncs, return_itc=False, n_jobs=-1)
        tfr_array[po].append(power)
        # decim=3,
        chanel_names = ['Fz','Cz', 'Pz', 'TP7', 'TP8']
        for chan in chanel_names:
            # Fourthly, time-frequency power was normalized with respect to a 400 to 200 ms prestimulus baseline and converted to decibels [10 × log (μV2)]
            fig = power.plot([chan], baseline=(-0.4, -0.2), mode='ratio', title='Morlet - Subject: ' + str(i+1) + ' Channel: ' + chan + ' Payoff: ' + po, show=False, dB=True)
            fig[0].savefig('plots/morlet_s' + str(i+1) + '_po_' + po + '_chan_' + chan + 'freq2ncs.png')
    #**N_CYCLES = coef * FREQS for several coef values**:
    #for coef in np.arange(0.1,1.5,0.1):
    #    print('coef', coef)
    #    for po in ['0','20']:
    #        print(po)
    #        chanel_names = ['Fz','Cz', 'Pz', 'TP7', 'TP8']
    #        power = tfr_morlet(epochs_resampled['PAYOFF_' + po], freqs=freqs, n_cycles=freqs*coef, return_itc=False, n_jobs=-1)
    #        for chan in chanel_names:
    #            # Fourthly, time-frequency power was normalized with respect to a 400 to 200 ms prestimulus baseline and converted to decibels [10 × log (μV2)]
    #            fig = power.plot([chan], baseline=(-0.4, -0.2), mode='ratio', title='Morlet - Subject: ' + str(i+1) + ' Channel: ' + chan + ' Payoff: ' + po + '_coef_' + str(coef), show=False, dB=True)
    #            fig[0].savefig('plots/morlet_s' + str(i+1) + '_po_' + po + '_chan_' + chan + '_coef_' + str(coef) + '.png')


print(tfr_array)
print(tfr_array['0'])
print(tfr_array['20'])

GA0 = grand_average(tfr_array['0'])
GA20 = grand_average(tfr_array['20'])


chanel_names = ['Fz','Cz', 'Pz', 'TP7', 'TP8']
for chan in chanel_names:
    fig = GA0.plot([chan], baseline=(-0.4, -0.2), mode='ratio', title='Morlet - GA - Channel: ' + chan + ' Payoff: 0', show=False, dB=True)
    fig[0].savefig('plots/morlet_GA_0_chan_' + chan + 'freq2ncs.png')

    fig = GA20.plot([chan], baseline=(-0.4, -0.2), mode='ratio', title='Morlet - GA - Channel: ' + chan + ' Payoff: 20', show=False, dB=True)
    fig[0].savefig('plots/morlet_GA_20_chan_' + chan + 'freq2ncs.png')

        # ANOTHER METHOD
        #print("multitaper")
        #time_bandwidth = 4
        #power = tfr_multitaper(epochs_resampled['PAYOFF_' + po], freqs, n_cycles, time_bandwidth=time_bandwidth, return_itc=False,n_jobs=-1)
        #for chan in chanel_names:
        #    fig = power.plot([chan], title='plots/Multitaper - Subject: ' + str(i) + ' Channel: ' + chan + ' Payoff: ' + po, show=False)
        #    fig[0].savefig('multitaper_s_' + str(i) + '_po_' + po + '_chan_' + chan + '.png')


    ## DONE: time-frequency representation for Fz / Cz / Pz / TP7 / TP8 (select these electrods)  
    ## DONE: TFR of epochs with id 2 and separetely epochs with id 6 (PAYOFF_0 vs PAYOFF_20)
    ## DONE: "Fourthly, time-frequency power was normalized with respect to a 400 to 200 ms prestimulus baseline and converted to decibels [10 × log (μV2)]."
    ## DONE: remove artifacts (cf section "Time-Frequency Power Datasets" in Trust Game Dataset paper): every +-7.5microV (and not peak to peak) ---> lambda
    ## DONE: GRAND AVERAGE ON SUBJECTS
    ## TODO: options de tfr + options de power.plot() (ex: freqs / n_cycle / time_bandwidth)
    ## PBM! valeurs entre 2 et -2 dans le papier (10 et -10 chez nous)
    ## PBM! mode "ratio" + dB=True => valeur proche de 0 entre -400 et -200 (ce n'est pas le cas dans le papier)
    ## PBM! le papier plot tfr de -200 à 1000
    ## TODO: (checker pourquoi mne version 1.3)
