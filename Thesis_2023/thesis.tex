\documentclass[12pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{url}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{eurosym}

\geometry{lmargin=1in, rmargin=1in, 
top=0.5in, 
%head=1.5in, 
headheight=0.1in,
bottom=1.6in, 
foot=0.5in
}
\fancypagestyle{plain}{\fancyhead[L]{\includegraphics[height=0.7in, keepaspectratio=true]{ISAE.png}} 
\fancyhead[C]{\hspace{-1.7cm}\includegraphics[height=0.7in, keepaspectratio=true]{Logo_EDSYS.png}}
\fancyhead[R]{\includegraphics[height=0.5in, keepaspectratio=true]{ONERA_logo.png}}}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}





% TITLE
\title{{\normalsize PhD Proposal in AI and Neuroscience}\\ 
\textit{ Towards Robust identification and Unraveling of human-System Trust correlates (TRUST)}}
% DCAS SUPERVISORS
\author{
Nicolas DROUGARD -- ISAE-SUPAERO\\ 
Bertille SOMON -- ONERA\\
}
\date{}



\begin{document}
\maketitle

\begin{itemize}
\item Start date of the thesis: October 2023 -- Duration: 36 months.
\item Key words: \textit{Trust, Human-System Interaction, EEG,
Passive Brain-Computer Interface, Machine Learning, Signal Processing.}
%\item Net salary: 1400\euro~per month.
\end{itemize}

\section{Introduction}
The increasing technology development
during the last decades
brings us to interact daily
with automated systems.
These systems are becoming
more and more reliable,
relegating operators to the role
of passive supervisors
and excluding them
from the control loop
\cite{berberian2017out}.
Moreover, their increasing complexity
has constrained the designers
of these systems
to make them more and more obscure,
imposing on the operators a blind trust
in the decisions made
\cite{dekker2002maba}.
However, trust in automated systems
poses major problems
because it is regularly
subject to calibration errors.
Numerous research studies,
both fundamental
and in ecological environments,
have demonstrated that this over-trust
in highly reliable systems
can be the cause of operational difficulties
such as the inability to detect
(infrequent) system errors
when they appear
\cite{wickens2021engineering}.
Similarly, under-trust,
or even mistrust,
can also be deleterious
and reduce the operational performance
of the operator-automated system couple
\cite{parasuraman1997humans}.
However, the mechanisms supporting
the emergence of trust
are still unclear and little studied.
In particular, the neurophysiological markers
associated with trust
are poorly identified and validated
\cite{ajenaghughrure2020measuring,
elkins2021comparison}.
The literature shows that
research on these correlates
has not focused on characterizing trust,
but on identifying its impact
on specific cognitive processes,
such as error detection
\cite{de2018learning}
or the feeling of control
\cite{hancock2011meta}.


%% difficulty ??
One of the difficulties
related to this characterization
comes from the field of
Brain-Computer Interfaces (BCI),
where promising advances
have been made in the last ten years
\cite{lotte2018review},
especially concerning passive BCIs
which remain a major challenge
in Machine Learning (ML)
based on brain activity measurements.
Indeed, ML techniques
are hard to implement
on neurophysiological data,
which are generally offered
in limited amounts,
often very noisy,
and which are subject to
a very high temporal
and inter-individual variability. 
An important field of BCI research
consists in using features
that have a physiological meaning,
in addition to their classification performance.
Obtaining this type of features
enables a better understanding
of the processes underlying complex phenomena.

\begin{figure}
\begin{center}
\includegraphics[scale=0.3]{eeg_classif.png}
\end{center}
\caption{Typical classification process in EEG-based BCI systems, from \cite{lotte2018review}.}
\label{pipeline}
\end{figure}

\section{Project}
In this thesis,
we seek to identify
the brain correlates of trust
in highly automated reliable systems,
as well as their variations over time,
based in particular on measurements
of brain activity
(electroencephalography, or EEG).
A major emphasis is put on the characterization,
understanding and evaluation
of trust in a transversal way
independently of the type of task
or cognitive process involved.
We therefore wish to determine
brain correlates of trust,
in the general sense of the term,
and which could be measured
in real-time on operators.\\

The contributions of this thesis
concern three challenges
at the interface between neuroscience
and artificial intelligence:
\begin{enumerate}
\item[--] The brain correlates of trust
that we wish to identify
must have good generalization properties,
\textit{i.e.} they must account
for the level of trust of the operator,
with robustness,
independently of the task performed
(invariance challenge);
\item[--] The selected markers
must allow a certain level
of explainability of the mechanisms
of emergence and variation of trust,
and thus have a physiological meaning
and plausible explanations
from the neuroscience literature,
in addition to their classification performances
(transparency challenge);
\item[--] The algorithms
for identifying and classifying markers
must be usable on online
and real-time measurements
in order to estimate
the temporal evolution of trust
(immediacy challenge).
\end{enumerate}

% 3 challenges
For short, the goal of this thesis
is to identify new objective markers
and techniques that are,
at the same time,
robust to non-trust related variability,
defined in a rather fundamental way,
and usable in real time
in an operational framework.\\


% WHAT DOES IT ALLOWS FOR??
% POTENTIAL RESULTS
%However, their identification through neurophysiological measurements could allow
% 1) a better understanding of the emergence of this confidence and its calibration,
% and 2) its
\subsection{Why?}
As it has been demonstrated
for other cognitive processes,
or \enquote{operator states},
we hypothesize that selection
and classification
of neurophysiological markers
could allow us to better define
the trust mechanism and explain its drifts,
thanks to the monitoring
of its evolution in real time,
and in different operating contexts. 
%By focusing on real-time measurements,
%these estimation methods
%allow the implementation of passive BCIs.
%Indeed, the estimation of the trust
%of human operators towards automated systems,
%makes possible the monitoring
%of this major operator mental state,
%with the aim of improving the performance
%of the operator-system couple. 
The estimation of this mental state,
or cognitive process,
is essential to obtain
a dynamic mental map of the operator,
\textit{i.e.} the estimation of mental states
of the operators in real time,
in order to allow a refined analysis
of the human-system missions,
or even to ensure a context favorable
to the success of such missions.
Indeed, the use of passive brain-computer interfaces
\cite{roy2022retrospective}
able to provide this kind of dynamic mental maps,
allows to develop and integrate
countermeasures in the system
(such as alarms, interaction mode changes,
or more generally reactions),
in order to extract the operator
from mission-deleterious mental states,
such as over- or under-trust. 


\subsection{How?}
Measuring and identifying brain correlates
and computational markers of trust
in automated systems
involves leveraging current available resources
in terms of neuroscience knowledge,
ML and BCI techniques, and EEG datasets.
%This thesis includes both upstream phases of evaluation 
% of the literature and experimental phases associated 
% with the development of algorithms for processing 
% and statistical analysis of data, acquired 
% or from "open access" platforms (eg MOABB or OSF).
%identify the computational markers and resources available
% NEURO -- neuroscience literature on trust + experiments
Indeed, an in-depth analysis
of the neuroscience literature
will be conducted in order to determine
the currently recognized markers of trust,
and their explainability
in terms of the cognitive processes involved,
as well as the experimental tasks performed in these studies.
%but also in terms of 
%in terms of brain markers, 
%, and thus associated cognitive processes.
% Datasets + experiments
Also, the selection of open access
EEG datasets,
associated with scientific publications
dealing with the identification
of trust correlates,
will allow to test,
under various conditions and tasks,
the reliability of the identified markers
through statistical analyses.
%Moreover, the interest of these databases will also be present during the development stage of 
%datasets accessible 
%completed by EEG data sets
%These datasets will 
After defining the associated experimental protocols,
new datasets will be created from the execution of experiments
with electroencephalographic (EEG) data acquisition
which will be conducted
at ONERA and/or ISAE-SUPAERO.
This data collection
will allow the reproduction of the results of the literature
as well as the identification of new markers.
%After defining the associated experimental protocols,
%the execution of experiments
%with electroencephalographic (EEG) data acquisition,
%that will be conducted
%at ONERA and/or at ISAE-SUPAERO,
%will allow a data collection reproducing
%and completing the results of the literature. %% PUBLICATION MOABB
%statistical analysis of the open access data and testing of the metrics identified in the literature 
%The markers most likely to account for absolute trust will then be evaluated in one or more experiments with electroencephalographic (EEG) data acquisition in order to identify their precise correlates.11
%implementation of the pre-tests, of the phases of experimental execution,
%, with the help of the collaboration with Bertille SOMON, 
% ML -- literature and libraries on BCI and machine learning (sklearn, MNE, MOABB, ...) + passive competition
Machine learning algorithms from the literature
will be trained on these two types of datasets,
those retrieved from the internet
and those recorded during the lab experiments,  
%allowing the continuous and real-time measurement of human-system trust.
%implementation of machine learning tools , 
using appropriate libraries
(e.g. scikit-learn \cite{scikit-learn}
and MNE \cite{GramfortEtAl2013a})
and evaluation (e.g. MOABB \cite{jayaram2018moabb}).
Figure \ref{pipeline} illustrates
classical BCI pipelines from raw EEG signals
to the mental state estimation,
\textit{i.e.} the trust level in our case. 
Newest techniques, including robust features extraction
(e.g. TDA features \cite{xu2021topological})
and data augmentation \cite{rommel2022data},
will be implemented.

%: selection and verification of data sets and algorithms.

%and of validation of the cerebral correlates and the associated metrics

%and in the experiments will then allow the training of machine learning algorithms on all the collected data.



%will be based on an in-depth analysis of the literature,
%will be based on these same acquisitions,
%Because of the different challenges (invariance, transparency and immediacy) addressed in this thesis, the approach envisaged will be based on two guidelines.
%In parallel, a second guideline will aim at 
%Finally, these tools and markers can be tested in a last experimental phase
%This experimental phase will have a more operational and applied aspect, in particular the problem of UAV supervision seems to be a good track for the evaluation of the trust of an operator during human-system interactions.
% SKLEARN + MNE + MOABB + 

%%% PLANNING
%The thesis program can be divided into 5 main stages over 36 months:
%1. Literature review (T0-T0+8): it will allow to 
%evaluate in more detail the current knowledge concerning the correlates of absolute confidence in automated systems both 
%This review will also 

%regarding the 
%and usable in the second phase of the thesis.
%2. Definition and implementation of the thesis experiments (T0+7-T0+13): 
%this work contains the usual steps of 

%then of 


%3. Collection of accessible data sets (T0+13-T0+16) and 

%4. Consolidation of the experimental results on the collected datasets (T0+16-T0+21): this phase of 

%5. Finally, a second experimental phase (with steps similar to point 2; T0+21-T0+30) will allow to consolidate the reliability of the markers and algorithms set up, on continuous and real-time brain activity measurements. One of the application fields anticipated for this second experimental phase would be the supervision of a light drone. Throughout the thesis, different moments of writing and publication of articles, which can be capitalized during the writing of the thesis (T0+30-T0+36), are to be expected, as well as participations in congresses.


%This work should lead to the identification of some markers and methods for estimating the absolute confidence level of automated systems. 
%They will be tested and validated on data from various 


The statistical analysis of the data
% from the experiments,
as well as %the performance of 
the Machine Learning tools implemented
will be part of the results of this thesis, 
as well as the documented datasets 
resulting from the experiments,
that could be shared using platforms
like MOABB \cite{jayaram2018moabb}.



\section{PhD candidate profile}
Candidates should have a
Master's degree in Cognitive Science,
or a Master's degree in Machine Learning
(or equivalent)
with experience in Signal Processing
and Statistics.
Programming skills (Python and MATLAB) are required.
Experience in electroencephalographic (EEG)
data acquisition would be appreciated
but is not mandatory.


\section{Application procedure}
Formal applications should include a detailed resume,
a motivation letter and transcripts of master's degree.
Samples of published research by the candidate
and reference letters are appreciated but not necessary.
Applications should be sent by email to 
\begin{itemize}
\item Nicolas DROUGARD (\texttt{firstname.lastname@isae-supaero.fr});
\item and Bertille SOMON (\texttt{firstname.lastname@onera.fr}).
\end{itemize}

% figure BCI pipeline + casque EEG

\scriptsize
\bibliographystyle{plain}
\bibliography{bibfile}
\end{document}
