Florian METZGER's internship

Work on the [dataset](https://datadryad.org/stash/share/I4_9eQgXJL0sjukj8I9ruT2TToTZ90RuZvSmJO5LnyY) from the article [Trust Game Database: Behavioral and EEG Data From Two Trust Games](https://www.frontiersin.org/articles/10.3389/fpsyg.2019.02656/full).

New folder with all the code and the report = 1_12_2022.

1/12/2022 Florian:
fig2 = mean of the window from 250 to 350
fig3 = mean of the window from 80 to 150
Pipeline with no human supervision of the data preprocessing/filtering (MNE does preprocessing/filtering by itself).

[Overleaf of the report](https://www.overleaf.com/project/6284a505b1b5f2811e1a97d2)

